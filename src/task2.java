import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class task2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a=in.nextInt();
        int b=in.nextInt();
        int c=in.nextInt();
        if(a%2==0&&(b%2==1||c%2==1))
            System.out.print( "YES" );
        else if(a%2==1&&(b%2==0||c%2==0))
            System.out.print( "YES" );
        else
            System.out.print( "NO" );
    }
}
